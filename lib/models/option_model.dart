import 'package:flutter/material.dart';

class Option {
  Icon icon;
  String title;
  String subtitle;

  Option({this.icon, this.title, this.subtitle});
}

final options = [
  Option(
    icon: Icon(Icons.add_alert, size: 40.0),
    title: 'Alerta de notificaciones',
    subtitle: 'Lorem ipsum dolor sit amet, consect.',
  ),
  Option(
    icon: Icon(Icons.people, size: 40.0),
    title: 'Perfil',
    subtitle: 'Lorem ipsum dolor sit amet, consect.',
  ),
  Option(
    icon: Icon(Icons.card_giftcard, size: 40.0),
    title: 'Método de Pago',
    subtitle: 'Lorem ipsum dolor sit amet, consect.',
  ),
  Option(
    icon: Icon(Icons.cloud_circle, size: 40.0),
    title: 'Nube Privada',
    subtitle: 'Lorem ipsum dolor sit amet, consect.',
  ),
  Option(
    icon: Icon(Icons.input, size: 40.0),
    title: 'Input',
    subtitle: 'Lorem ipsum dolor sit amet, consect.',
  ),
  Option(
    icon: Icon(Icons.fastfood, size: 40.0),
    title: 'Servicio de Comida',
    subtitle: 'Lorem ipsum dolor sit amet, consect.',
  ),
  Option(
    icon: Icon(Icons.gamepad, size: 40.0),
    title: 'Juegos',
    subtitle: 'Lorem ipsum dolor sit amet, consect.',
  ),
];
